import math
import random
import sys

def split(filename, typeEnd, percentage, startType):
    with open(filename) as f:
        r = f.readlines()
    
    trainingData = int(math.ceil(percentage*len(r)))
    randomNos = random.sample(range(0, len(r)), trainingData)
    notChosen = set(range(0,len(r))) - set(randomNos)
    notChosen = list(notChosen)

    traindata = open(startType+"train_"+typeEnd,"w")

    for indexes in randomNos:
        traindata.write(r[indexes])

    traindata.close()

    testdata = open(startType+"test_"+typeEnd,"w")

    for inds in notChosen:
        testdata.write(r[inds])

    testdata.close()

    print ('Done!')



split('Email Data/NB Data/spam.nb.feat','25.nb',.25,'Email Data/NB Data/')
split('Email Data/NB Data/spam.nb.feat','75.nb',.75,'Email Data/NB Data/')

split('Email Data/SVM Data/spam.svm.feat','25.svm',.25,'Email Data/SVM Data/')
split('Email Data/SVM Data/spam.svm.feat','75.svm',.75,'Email Data/SVM Data/')

split('Email Data/MegaM Data/spam.megam.feat','25.megam',.25,'Email Data/MegaM Data/')
split('Email Data/MegaM Data/spam.megam.feat','75.megam',.75,'Email Data/MegaM Data/')


split('IMDB Data/NB Data/sentiment.nb.feat','25.nb',.25,'IMDB Data/NB Data/')
split('IMDB Data/NB Data/sentiment.nb.feat','75.nb',.75,'IMDB Data/NB Data/')

split('IMDB Data/SVM Data/sentiment.svm.feat','25.svm',.25,'IMDB Data/SVM Data/')
split('IMDB Data/SVM Data/sentiment.svm.feat','75.svm',.75,'IMDB Data/SVM Data/')

split('IMDB Data/MegaM Data/sentiment.megam.feat','25.megam',.25,'IMDB Data/MegaM Data/')
split('IMDB Data/MegaM Data/sentiment.megam.feat','75.megam',.75,'IMDB Data/MegaM Data/')