fileIp = 'result.sentiment'

opD = fileIp.split('.')

pos =''
neg = ''

with open(fileIp) as f:
	r = f.readlines()

op = open('sentiment.megam.post.out','w')
for lines in r:
	line = lines.strip()
	line1 = line.split()

	if 'spam' in opD:
		pos = 'SPAM'
		neg = 'HAM'
	else:
		pos = 'POSITIVE'
		neg = 'NEGATIVE'		
	if line1[0] == '1':
		op.write(pos+"\n")
	else:
		op.write(neg+"\n")
op.close()

