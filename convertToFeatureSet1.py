import glob
import sys
import random

def createEmail(vocabList,readFiles):
	with open('spam.nb.train',"w") as outfile:
		count = 0
		for f in readFiles:
			count += 1
			print (count)
			countDictionary = {}
			with open(f,"r",encoding = 'latin1') as inputFile:
				print ('Reading File: ',f)
				readL = inputFile.readlines()
				for lines in readL:
					tokens = lines.split() 
					for token in tokens:
						token1 = token.lower()
						if token1 in countDictionary:
							countDictionary[token1]['count'] += 1
						else:
							if token1 in vocabList:
								countDictionary[token1] = {'count':1,'index':vocabList.index(token1)+1}
							else:
								continue
				sentence = {}
				for t in countDictionary:
					sentence[countDictionary[t]['index']] = str(countDictionary[t]['count'])
				pathSplit = f.split('/')
				writeToFile = ''
				if 'spam' in pathSplit:
					print ('Classified as spam.')
					writeToFile = 'SPAM '
				else:
					print ('Classified as ham.')
					writeToFile = 'HAM '
				for sent in sorted(sentence):
					writeToFile += str(sent)+':'+str(sentence[sent])+" "

				outfile.write(writeToFile+"\n")
	print ('Finished generating file for email.')

''' Email for Labeled Data'''
def createEmailSvmAndMegaM(nbFile,opFile,cType, pF , nF):
	outFile = open(opFile,'w')
	with open(nbFile,encoding='latin1') as f:
		for lines in f:
			line = lines.split()
			check = line[0]
			if check == 'SPAM':
				writeToFile = pF
			else:
				writeToFile = nF
			for token in line[1:]:
				ind, count = token.split(':')
				if cType == 'megam':
					writeToFile += ind+' '+count+" "
				else:
					writeToFile += ind+':'+count+" "
			outFile.write(writeToFile+"\n")
		outFile.close()
	print ('Finished generating file for email.')

''' Email for Labeled Data'''
def emailFeatureFileGeneratorWithLabel():
	global r
	r = open('enron.vocab','r',encoding = 'latin1') 
	emailVocabList = []
	for vocab in r:
		vocab1 = vocab.strip()
		emailVocabList.append(vocab1.lower())

	read_files = []
	i = ['1','2','4','5']
	for c in i:
		read_files += glob.glob('data/enron'+c+'/spam/*')
		read_files += glob.glob('data/enron'+c+'/ham/*')
	readF = sorted(read_files)

	createEmail(emailVocabList,readF)
	createEmailSvmAndMegaM('spam.nb.feat','Email Data/SVM Data/spam.svm.feat','svm','+1 ','-1 ')
	createEmailSvmAndMegaM('spam.nb.feat','Email Data/MegaM Data/spam.megam.feat','megam','1 ','0 ')

	print ("Finished generating models for email.")

''' Sentiment for Labeled Data'''
def createImdbFeaturesWithLabel(outputFile, pF, nF):
	opD = outputFile.split('.')
	global r1
	count = 0
	with open(outputFile,"w") as fileWrite:
		count += 1
		print ('Read Line number: ',count)
		for lines in r1:
			feat = ''
			line = lines.split()
			rating = int(line[0])
			if rating >= 7:
				feat = pF
			elif rating <= 4:
				feat = nF
			sentence1 = feat
			for tokens in line[1:]:
				token ,count = tokens.split(':')
				token = int(token)
				if 'megam' in opD:
					sentence1 += str(token+1)+' '+str(count)+" "
				elif 'svm' in opD:
					sentence1 += str(token+1)+':'+str(count)+" "
				else:
					sentence1 += str(token+1)+':'+str(count)+" "

			fileWrite.write(sentence1+"\n")
	fileWrite.close()
	print ('Generated feature file for: ',opD[1])

''' Sentiment for Labeled Data'''
def imdbFeatureFileGeneratorWithLabel():
	global r1
	with open('labeledBow.feat','r',encoding = 'latin1') as f:
		r1 = f.readlines()

	createImdbFeaturesWithLabel('IMDB Data/NB Data/sentiment.nb.feat','POSITIVE ', 'NEGATIVE ')
	createImdbFeaturesWithLabel('IMDB Data/SVM Data/sentiment.svm.feat','+1 ', '-1 ')
	createImdbFeaturesWithLabel('IMDB Data/MegaM Data/sentiment.megam.feat','1 ', '0 ')

	print ('Finished generating models IMDB data.')

def createEmailNoLabel(vocabList,readFiles):
	with open('Test/spam_test.nb.train',"w") as outfile:
		count = 0
		for f in readFiles:
			count += 1
			print (count)
			countDictionary = {}
			with open(f,"r",encoding = 'latin1') as inputFile:
				print ('Reading File: ',f)
				readL = inputFile.readlines()
				for lines in readL:
					tokens = lines.split() 
					for token in tokens:
						token1 = token.lower()
						if token1 in countDictionary:
							countDictionary[token1]['count'] += 1
						else:
							if token1 in vocabList:
								countDictionary[token1] = {'count':1,'index':vocabList.index(token1)+1}
							else:
								continue
				sentence = {}
				for t in countDictionary:
					sentence[countDictionary[t]['index']] = str(countDictionary[t]['count'])
				writeToFile = ''
				for sent in sorted(sentence):
					writeToFile += str(sent)+':'+str(sentence[sent])+" "

				outfile.write(writeToFile+"\n")
	print ('Finished generating file for email.')

''' Email for Labeled Data'''
def createEmailSvmAndMegaMNoLabel(nbFile,opFile,cType, pF , nF):
	outFile = open(opFile,'w')
	mm = ['1 ']
	svm = ['+1 ']
	with open(nbFile,encoding='latin1') as f:
		for lines in f:
			writeToFile = ''
			line = lines.split()
			if cType == 'megam':
				writeToFile = random.choice(mm)
			else:
				writeToFile = random.choice(svm)
			for token in line:
				ind, count = token.split(':')
				if cType == 'megam':
					writeToFile += ind+' '+count+" "
				else:
					writeToFile += ind+':'+count+" "
			outFile.write(writeToFile+"\n")
		outFile.close()
	print ('Finished generating file for email.')

''' Unlabeled Email '''
def emailFeatureFileGenerator():
	global r
	
	r = open('enron.vocab','r',encoding = 'latin1')
	
	global countDictionary 
	emailVocabList = [] 
	for vocab in r:
		vocab1 = vocab.strip()
		emailVocabList.append(vocab1.lower())

	read_files = glob.glob('spam_or_ham_test/*')
	readF = sorted(read_files)
	
	#createEmailNoLabel(emailVocabList,readF)
	createEmailSvmAndMegaMNoLabel('Feature_Files/spam_test.nb.feat','spam_test.svm.feat','svm','+1 ','-1 ')
	createEmailSvmAndMegaMNoLabel('Feature_Files/spam_test.nb.feat','spam_test.megam.feat','megam','1 ','0 ')

	print ("Finished generating models for email.")


''' Unlabeled Sentiment '''
def createImdbFeatures(outputFile, pF, nF):
	opD = outputFile.split('.')
	global r1
	count = 0
	mm = ['1 ']
	svm = ['+1 ']
	with open(outputFile,"w") as fileWrite:
		for lines in r1:
			feat = ''
			line = lines.split()
			sentence1 = ""
			if 'megam' in opD:
				sentence1 = random.choice(mm)
			elif 'svm' in opD:
				sentence1 = random.choice(svm)
			for tokens in line:
				token ,count = tokens.split(':')
				token = int(token)
				if 'megam' in opD:
					sentence1 += str(token+1)+' '+str(count)+" "
				elif 'svm' in opD:
					sentence1 += str(token+1)+':'+str(count)+" "
				else:
					sentence1 += str(token+1)+':'+str(count)+" "

			fileWrite.write(sentence1+"\n")
	fileWrite.close()
	print ('Generated feature file for: ',opD[1])

''' Unlabeled Sentiment '''
def imdbFeatureFileGenerator():
	global r1
	with open('sentiment_test.feat','r',encoding = 'latin1') as f:
		r1 = f.readlines()

	createImdbFeatures('Feature_Files/sentiment_test.nb.feat','POSITIVE ', 'NEGATIVE ')
	createImdbFeatures('Feature_Files/sentiment_test.svm.feat','+1 ', '-1 ')
	createImdbFeatures('Feature_Files/sentiment_test.megam.feat','1 ', '0 ')

	print ('Finished generating models IMDB data.')


countDictionary = {}
vocabList = []
r = []
r1 = []
read_files = []


option = input("Labeled Data:1 Unlabeled Data:2.\n")
if option == '1':
	print ('Generating feature files for Email Data.')
	emailFeatureFileGeneratorWithLabel()
	print ('Generating feature files for IMDB Data.')
	imdbFeatureFileGeneratorWithLabel()
else:
	print ('Generating feature files for Email Data.')
	emailFeatureFileGenerator()
	print ('Generating feature files for IMDB Data.')
	imdbFeatureFileGenerator()


# ''' Unlabeled Email '''
# def createEmailFeatures(read_files,outputFileName, pF , nF):
# 	opD = outputFileName.split('.')
# 	global vocabList	
# 	global countDictionary
# 	count = 0
# 	with open(outputFileName,"w") as outfile:
# 		for f in read_files:
# 			count += 1
# 			print (count)
# 			countDictionary = {}
# 			with open(f,"r",encoding = 'latin1') as inputFile:
# 				print ('Reading File: ',f)
# 				readL = inputFile.readlines()
# 				for lines in readL:
# 					tokens = lines.split() 
# 					for token in tokens:
# 						token1 = token.lower()
# 						if token1 in countDictionary:
# 							countDictionary[token1]['count'] += 1
# 						else:
# 							if token1 in vocabList:
# 								countDictionary[token1] = {'count':1,'index':vocabList.index(token1)+1}
# 							else:
# 								continue
# 				sentence = {}
# 				for t in countDictionary:
# 					sentence[countDictionary[t]['index']] = str(countDictionary[t]['count'])

# 				pathSplit = f.split('/')
# 				writeToFile = ''

# 				for sent in sorted(sentence):
# 					if 'megam' in opD:
# 						writeToFile += str(sent)+' '+str(sentence[sent])+" "
# 					elif 'svm' in opD:
# 						writeToFile += str(sent)+':'+str(sentence[sent])+" "
# 					else:
# 						writeToFile += str(sent)+':'+str(sentence[sent])+" "

# 				outfile.write(writeToFile+"\n")
# 	print ('Finished generating file for email.')