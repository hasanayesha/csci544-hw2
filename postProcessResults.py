result = 'sentiment.megam.25.post.out'
with open(result) as f:
	resultData = f.readlines()

opD = result.split('.')

testFile = 'test_25.megam'
with open(testFile) as p:
	testData = p.readlines()

res = ''


correctPositive = 0.0
correctNegative = 0.0

precisionCounterP = 0.0
precisionCounterN = 0.0

recallCounterP = 0.0
recallCounterN = 0.0 


for ind,resultlines in enumerate(resultData):
	calcResult = resultlines.strip()
	actualResult = testData[ind]
	actual = actualResult.split()
	if actual[0] == '1':
		act = 'POSITIVE'
		recallCounterP += 1.0
	else:
		act = 'NEGATIVE'
		recallCounterN += 1.0

	if calcResult == 'POSITIVE':
		precisionCounterP += 1.0
		if calcResult == act:
			correctPositive += 1.0
	else:
		precisionCounterN += 1.0
		if calcResult == act:
			correctNegative += 1.0
	
positivePrecision = correctPositive/precisionCounterP
negativePrecision = correctNegative/precisionCounterN


positiveRecall = correctPositive/recallCounterP
negativeRecall = correctNegative/recallCounterN


print ('=================================')
print ("Precision: ")
print ("Positive: ", positivePrecision*100.0)
print ("Negative: ", negativePrecision*100.0)

print ("=================================")
print ("Recall: ")
print ("Positive: ", positiveRecall*100.0)
print ("Negative: ", negativeRecall*100.0)


f1Postive = (2*positivePrecision*positiveRecall)/(positivePrecision + positiveRecall)
f1Negative = (2*negativePrecision*negativeRecall)/(negativeRecall + negativePrecision)

print ('=================================')
print ("F1 Score")
print ("F1 Positive: ", f1Postive*100.0)
print ("F1 Negative: ", f1Negative*100.0)

print ('=================================')