fileIp = 'sentiment.svm.out'

opD = fileIp.split('.')

pos =''
neg = ''

with open(fileIp) as f:
	r = f.readlines()

op = open('sentiment.svm.post.out','w')
for lines in r:
	line = lines.strip()
	numb = float(line)
	if 'spam' in opD:
		pos = 'SPAM'
		neg = 'HAM'
	else:
		pos = 'POSITIVE'
		neg = 'NEGATIVE'		
	if numb > 0.0:
		op.write(pos+"\n")
	else:
		op.write(neg+"\n")
op.close()

