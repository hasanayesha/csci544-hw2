import sys
import math

def predictClassWithLabel(testData, p1, n1):
    global readModel
    global calculatedData
 
    correctPositive = 0.0
    correctNegative = 0.0

    precisionCounterP = 0.0
    precisionCounterN = 0.0

    recallCounterP = 0.0
    recallCounterN = 0.0   
    for lines in testData:
        sentence = lines.split()
        rating = sentence[0]
        if rating == p1:
            recallCounterP += 1.0
        elif rating == n1:
            recallCounterN += 1.0
        probPositive = float(readModel[2])
        probNegative = float(readModel[3])

        for tokens in sentence[1:]:
            token, count = tokens.split(':')
            if token in calculatedData:
                probPositive += calculatedData[token]['pos']*float(count)
                probNegative += calculatedData[token]['neg']*float(count)

        if probPositive > probNegative:
            precisionCounterP += 1.0
            if rating == p1:
                correctPositive += 1.0
        else:
            precisionCounterN += 1.0
            if rating == n1:
                correctNegative += 1.0

    print (correctPositive,correctNegative)
    print (precisionCounterP,precisionCounterN)
    print (recallCounterP,recallCounterN)

    positivePrecision = correctPositive/precisionCounterP
    negativePrecision = correctNegative/precisionCounterN


    positiveRecall = correctPositive/recallCounterP
    negativeRecall = correctNegative/recallCounterN


    print ('=================================')
    print ("Precision: ")
    print ("Positive: ", positivePrecision*100.0)
    print ("Negative: ", negativePrecision*100.0)

    print ("=================================")
    print ("Recall: ")
    print ("Positive: ", positiveRecall*100.0)
    print ("Negative: ", negativeRecall*100.0)


    f1Postive = (2*positivePrecision*positiveRecall)/(positivePrecision + positiveRecall)
    f1Negative = (2*negativePrecision*negativeRecall)/(negativeRecall + negativePrecision)

    print ('=================================')
    print ("F1 Score")
    print ("F1 Positive: ", f1Postive*100.0)
    print ("F1 Negative: ", f1Negative*100.0)

    print ('=================================')

def predictClassWithoutLabel(testData, p1, n1):
    global readModel
    global calculatedData

    for lines in testData:
        sentence = lines.split()
        probPositive = float(readModel[2])
        probNegative = float(readModel[3])

        for tokens in sentence:
            token, count = tokens.split(':')
            if token in calculatedData:
                probPositive += calculatedData[token]['pos']*float(count)
                probNegative += calculatedData[token]['neg']*float(count)

        if probPositive >= probNegative:
            print(p1)
        else:
            print(n1)


if len(sys.argv) > 3 or len(sys.argv) < 3:
    print ('Usage: python3 nbclassify.py model_file  test_file')
    sys.exit(1)

else:
    modelFile = sys.argv[1]
    testFile = sys.argv[2]

calculatedData = {}

with open(modelFile) as r:
    readModel = r.readlines()

with open(testFile) as m:
    testData = m.readlines()


for lines in readModel[4:]:
    try:
        toke , pos, neg = lines.split()
        calculatedData[toke]={'pos':float(pos),'neg':float(neg)}
    except ValueError:
        print ('Error: ', lines)

p1 = ""
n1 = ""

typeOfFile = readModel[1].strip()
if typeOfFile == 'SPAM':
    p1 = "SPAM"
    n1 = "HAM"
else:
    p1 = "POSITIVE"
    n1 = "NEGATIVE"

testD = testData[0].split()
if testD[0] == 'SPAM' or testD[0] == 'HAM' or testD[0] == 'POSITIVE' or testD[0] == 'NEGATIVE':
    predictClassWithLabel(testData,p1,n1)
else:
    predictClassWithoutLabel(testData,p1,n1)
