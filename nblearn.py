import math
import random
import sys

def learnWithLabel(inputData, modelfile,positive,negative):
    modelWrite = open(outputfile,'w')

    vocabCounter = {}
    positiveWords = 0.0
    negativeWords = 0.0
    positiveSentences = 0.0
    negativeSentences = 0.0
    
    #countOfLines = 0
    for lines in inputData:
        #countOfLines += 1
        #print ('line no: ',countOfLines)
        sentence = lines.split()
        rating = sentence[0]
        if rating == positive:
            positiveSentences += 1.0    
            for tokens in sentence[1:]: 
                token, count = tokens.split(':') 
                positiveWords += float(count)
                if token in vocabCounter:
                    vocabCounter[token]['positive'] += float(count)
                else:
                    vocabCounter[token] = {'positive':float(count),'negative':0.0}
    
        elif rating == negative:
            negativeSentences += 1.0
            for tokens in sentence[1:]:
                token, count = tokens.split(':') 
                negativeWords += float(count)
                if (token in vocabCounter):
                    vocabCounter[token]['negative'] += float(count)
                else:
                    vocabCounter[token] = {'positive':0.0,'negative':float(count)}
    
    print (positiveWords , negativeWords)
    print (positiveSentences, negativeSentences)

    vocabSize = len(vocabCounter)
    modelWrite.write(str(vocabSize)+"\n")
    modelWrite.write(positive+"\n")
    totalWords = positiveSentences+negativeSentences
    tPos = math.log(positiveSentences/totalWords)
    tNeg = math.log(negativeSentences/totalWords)
    modelWrite.write(str(tPos)+"\n")
    modelWrite.write(str(tNeg)+"\n")
    
    for tokens in vocabCounter:
        try:
            pos = vocabCounter[tokens]['positive']/positiveWords
            pos = math.log(pos)
        except ValueError:
        	pos = (vocabCounter[tokens]['positive']+1)/(positiveWords+vocabSize)
        	pos = math.log(pos)
        try:
        	neg = vocabCounter[tokens]['negative']/negativeWords
        	neg = math.log(neg)
        except ValueError:
        	neg = (vocabCounter[tokens]['negative']+1)/(negativeWords+vocabSize)
        	neg = math.log(neg)
    
        modelWrite.write(str(tokens)+" "+str(pos)+" "+str(neg)+"\n")
    
    modelWrite.close()



if len(sys.argv) < 3 or len(sys.argv) > 3:
    print ('Usage: python3 nblearn.py training_file model_file')
    sys.exit(1)
 
else:
    inputfile = sys.argv[1]
    outputfile = sys.argv[2]

with open(inputfile,encoding="latin1") as f:
    inputData = f.readlines()

data = inputData[0].split()
print (data[0])
if data[0] =='SPAM' or data[0] == 'HAM':
    pos1 = 'SPAM'
    neg1 = 'HAM'
else:
    pos1 = 'POSITIVE'
    neg1 = 'NEGATIVE'

learnWithLabel(inputData,outputfile,pos1,neg1)